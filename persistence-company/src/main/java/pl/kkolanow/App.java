package pl.kkolanow;

import pl.kkolanow.model.Employee;
import pl.kkolanow.model.Phone;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kkolanow
 */
public class App {


    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("company");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {
            entityManager.getTransaction().begin();
            Employee e = new Employee();
            e.setName("Jan");
            e.setSalary(1000);
            List<Phone> phones = new ArrayList<Phone>();
            phones.add(PhoneGen.createPhone());
            e.setPhones(phones);
            entityManager.persist(e);
            entityManager.getTransaction().commit();

            List<Employee> list = entityManager.createQuery("select e from Employee e").getResultList();
            for(Employee emp: list) {
                System.out.println(emp.toString());
            }


        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }

        entityManager.close();


    }
}
